//PLEASE NOTE: A background in chemistry is required to understand this program
//Furthermore this is not a simple algorithm, and it will take time to understand
//This program performs 1 of 2 functions
//It can either find the pH of a solution that is completely known
//or it can find the concentration of an otherwise known solution
//This is the second iteration of the ph calculator
//Like the original, the user can add any number of acids or bases to a solution
//In order to calculate pH we do the following
//First a guess ph is assumed
//Then we can calculate how much each acid and base will dissociate
//Each acid or base has an "h_contribution" which is how much H they add to the solution
//Bases have a negative h_concentration
//Once the h_contributions are totalled it may be negative
//Now we consider the self ionization of water to determine a positive value for [H]
//We finally we use this number to determine the a pH
//If this number is the same as the original guess, than our original guess is wrong
//Clearly we're a guess such that guess_ph=calculated_ph
//By rearranging guess_ph-calculated_ph=0
//This equation is a 1D equation that can be solved via bisection
//https://en.wikipedia.org/wiki/Bisection_method
//Although there are other 1D equation solvers, the bisection method is the most reliable
//To compute the concentration of an unknown is much faster
//What we do is we effectively reverse engineer the way we calculated pH
//From the pH we know [H]
//From this we can get the total h_contribution
//If we subtract the contribution of everything else we get the contribution from the unknown
//From the contribution and pH we can solve for concentration
//Contribution increases linear with concentration, so we can do the calculation with a
//concentration of 1, then extrapolate what the concentration is supposed to be
mod io_functions;
use io_functions::*;
mod calculations;
use calculations::acid_or_base::*;
//This file contains a single function, main which gives a top-down view of the program
///main one by one reads in the the acids and bases
///A single acid or base can have unknown concentration
///After each one the user is asked if they are finished
///Once they are done, if there were no unknowns the pH is displayed
///Otherwise they'll be asked for the ph, and the concentration of the unknown is calculated
///This function uses a bunch of functions from io_functions
///Functions of the form get_thing() parse standard input to get the value
///and print error messages if the formatting is incorrect
fn main() {
	let mut solution: Vec<AcidOrBase> = Vec::new();
	//The solution is all the acids and bases
	//The solution vector consists of all acids and bases that are completely known
	//Additionally there can be up to a single acid or base with unknown concentration
	let mut unknown: Option<AcidOrBase> = None;
	loop {
		//This loop reads in all the user input
		//It does it in 2 steps, first it finds the type and k values
		//then it finds the concentration
		println!(
			"Enter the chemical formula of the {} compound:",
			if solution.len() == 0 && unknown.is_none() { "first" } else { "next" }
		);
		//This match block finds the type and k values
		//First it looks up the name, and if it can't find it it'll ask the user for details
		let mut next_compound = match AcidOrBase::dictionary_lookup(get_line()) {
			Some(x) => x,
			None => {
				//we begin by attempting to find it in the list of known compounds,
				//otherwise we need to ask the user
				//for the purposes of unwrap_or, asking the user is considered the default case
				println!("That compound is not recognised");
				//First we ask them for what type it is
				println!("Is it an acid (a) or a base (b)?");
				//Then we read it in
				let is_acid = get_is_acid();
				//Then we prompt the user for the k values
				if is_acid {
					println!("Enter the Ka value(s):");
				}
				else {
					println!("Enter the Kb value(s):");
				}
				//Lastly we read them in, and construct the object
				AcidOrBase::new(is_acid, get_k_values())
				//The constructor takes 2 attributes, we'll set concentration later
			}
		};
		println!("Enter the concentration:");
		match get_concentration(unknown.is_none()) {
			Some(x) => {
				//If we get a concentration then we are all set and can add it to solution
				next_compound.concentration = x;
				solution.push(next_compound);
			}
			None => {
				//If there's already an unknown then the user has found a way to cheat the system
				assert!(unknown.is_none());
				unknown = Some(next_compound);
			}
		}
		println!("Is there anything else in solution? (Y/n)");
		if !get_is_continuing() {
			break;
		}
	}
	match unknown {
		//Here we show the final result
		Some(compound) => {
			//Otherwise we'll ask them what the pH was
			println!("Enter the pH:");
			println!(
				"concentration = {:.1e}",
				calculations::find_concentration(compound, solution, get_ph())
			);
		}
		None => {
			//If everything is known we print the pH
			println!("pH = {:.1}", calculations::ph_of_solution(solution));
		}
	}
}
