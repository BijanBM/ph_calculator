//At quite a few points throughout this program, we need to keep bugging the user until
//they enter properly formatted input
//These function do not print the initial prompt i.e. "Enter the pH"
//but they do print the error messages i.e. "Enter an actual number"
//This is to make the main function easier to understand
//It would be beneficial to read main.rs first for context
///get_line returns a single line from standard input
///It removes leading and trailing whitespace and converts the input to lowercase
///This is to make the interface more insensitive
///Every other function in this file uses this one
pub fn get_line() -> String {
	//this function reads in a single line from standard input, which the program does a lot of
	//it also converts it all to uppercase and removes leading and trailing whitespace
	let mut input = String::new();
	std::io::stdin().read_line(&mut input).unwrap();
	input.trim().to_lowercase().to_string()
}
///get_concentration reads a non-negative number from standard input or a question mark
///? means the concentration is unknown
///Since there can only be a single unknown, this function needs to know if ? is still acceptable
///It reads lines one at a time until it gets a properly formatted line
///If the input isn't a number or a '?' it'll print: "Enter an actual number or '?'"
///The "or '?'" clause is omitted if it can't actually be unknown
///If the input is negative it'll print: "Concentration can't be negative"
///If the user attempts to enter a second ?, it'll print "There can't be 2 unknowns"
///If they use a question mark properly it'll return None
///Otherwise it'll convert the input to f64 and return it
pub fn get_concentration(can_be_unknown: bool) -> Option<f64> {
	loop {
		//the loop runs once for each line get_concentration needs to read
		let input = get_line();
		if input == "?" {
			if can_be_unknown {
				//This is proper use of ?
				return None;
			} else {
				//This is improper use of ?
				println!("There can't be 2 unknowns");
				continue;
			}
		}
		match input.parse::<f64>() {
			Ok(x) => {
				if x >= 0.0 {
					//This means everything worked
					return Some(x);
				} else {
					//This means they entered a negative number
					println!("Concentration can't be negative");
				}
			}
			//This means they didn't enter a number
			//We'll have to print one of 2 error messages
			Err(_) => println!(
				"Enter an actual number{}",
				if can_be_unknown { " or '?'" } else { "" }
			),
		}
	}
}
///get_ph is similar to get_concentration, but much simpler
///It returns a single number from standard input, which can be negative or positive
pub fn get_ph() -> f64 {
	loop {
		match get_line().parse::<f64>() {
			Ok(x) => return x,
			Err(_) => println!("Enter an actual number"),
		}
	}
}
///get_k_values returns a vector of f64s from standard input
///the user is to enter all the values on a single line, separated by whitespace
///if they enter something that isn't a number, or is isn't positive, we have to ask them again
///for example if the user enters:
///inf 0.12 3.45e-5
///it'll return vec![INFINITY, 0.12, 0.0000345]
///if they write:
///0.01 es 0.02
///it'll print: "es is not a number", and they'll have to enter the values again
///if they write
///0 2.1
///it'll print: "k values must be positive", and they'll have to enter the values again
pub fn get_k_values() -> Vec<f64> {
	'scan: loop {
		//the scan loop runs once for each time we get user input
		let mut result = Vec::new(); //Result accumulates the input values
		for s in get_line().split_whitespace() {
			match s.parse::<f64>() {
				//first we check the input is a number
				Err(_) => {
					//if it's not a number we tell them that, then repeat the process
					println!("{} is not a number", s);
					continue 'scan;
				}
				Ok(x) => {
					if x > 0.0 {
						//if everything is correct, we'll add it to our result vector
						result.push(x);
					} else {
						//if the user enters a negative number, we'll tell them and start over
						println!("k values must be positive");
						continue 'scan;
					}
				}
			}
		}
		return result;
	}
}
///get_is_acid determines whether the compound is an acid or base
pub fn get_is_acid() -> bool {
	loop {
		match get_line().as_ref() {
			"a" | "(a)" | "'a'" | "acid" => {
				return true;
			}
			"b" | "(b)" | "'b'" | "base" => {
				return false;
			}
			_ => {
				println!("Please enter 'a' or 'b'");
			}
		}
	}
}
///get_is_continuing determines whether the user wishes to add to the solution or if they're done
///The user may use a single letter "y" or "n" or the may write out the whole word "yes" or "no"
///If they enter nothing the default behavior is to return true
pub fn get_is_continuing() -> bool {
	loop {
		match get_line().as_ref() {
			//if the user is finished, then the main loop is exited
			//if however they are not finished, the inner loop is exited
			//and another iteration of the main loop begins
			"y" | "'y'" | "(y)" | "yes" | "" => return true,
			//"" is an option since yes is considered the default
			"n" | "'n'" | "(n)" | "no" => return false,
			_ => println!("Please enter 'y' or 'n'"),
		}
	}
}