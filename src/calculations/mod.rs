//Once the input is read and interpreted,
//The functions in this file become responsible for doing the calculation
//It would be beneficial to read main.rs first for context
pub mod acid_or_base;
use self::acid_or_base::AcidOrBase;
///This is the function that outputs the final ph of the solution
///It uses the method outlined at the beginning of main.rs
///It makes a single call to solve()
pub fn ph_of_solution(solution: Vec<AcidOrBase>) -> f64 {
	solve(
		|x| x + apply_self_ionization(solution.iter().map(|s| s.h_contribution(x)).sum()).log10(),
		(-10.0, 24.0),
		1e-2,
	)
}
///find_concentration takes 3 inputs, an acid or base with known identity, but unknown concentration
///a solution vector consisting of everything else in solution, and the pH
///find_concentration returns the concentration of the unknown
pub fn find_concentration(unknown: AcidOrBase, solution: Vec<AcidOrBase>, ph: f64) -> f64 {
	//total_dissociation is [H] being added to the solution minus [OH]
	//before considering self ionization
	let total_dissociation = undo_self_ionization(10_f64.powf(-ph));
	//known_dissociation is how much of total dissociation doesn't come from the unknown
	let known_dissociation = solution.iter().map(|x| x.h_contribution(ph)).sum::<f64>();
	//unknown_dissociation is how much comes from the unknown
	let unknown_dissociation = total_dissociation - known_dissociation;
	//If we double concentration, we double h_contribution
	//What this means is we can find the concentration by first finding the h_contribution
	//with a concentration of 1, then dividing unknown_dissociation to get the true concentration
	unknown_dissociation / AcidOrBase {
		concentration: 1.0,
		..unknown
	}.h_contribution(ph)
}
///In a solution with a given h_concentration and an OH concentration of 0,
///What would be the final concentration of H at equilibrium given the self ionization of water?
///This function answers this question
///Note that in a basic solution the h_concentration will start in the negatives
fn apply_self_ionization(h_concentration: f64) -> f64 {
	(h_concentration + (h_concentration.powi(2) + 4e-14).sqrt()) / 2.0
}
///This function does the reverse of apply_self ionization
///This will become useful when finding a concentration or k value
///If [H] is known at equilibrium, then there's some other solution in which [OH]=0
///which has the same equilibrium
///This function finds [H] of this other solution
fn undo_self_ionization(h_concentration: f64) -> f64 {
	h_concentration - 1e-14 / h_concentration
}
///This function finds the solution to a function on a given interval
///The error is the maximum difference between the return value and the solution
///It does this using the bisection method
///https://en.wikipedia.org/wiki/Bisection_method
fn solve<T: Fn(f64) -> f64>(f: T, mut interval: (f64, f64), error: f64) -> f64 {
	assert_ne!(f(interval.0).signum(), f(interval.1).signum());
	//Here we assert that it is a valid interval
	loop {
		//This loop shrinks the interval until it is adequately small
		let mid = (interval.0 + interval.1) / 2.0;
		if (interval.1 - interval.0).abs() / 2.0 < error {
			return mid;
		}
		if f(mid).signum() == f(interval.0).signum() {
			interval.0 = mid;
		} else {
			interval.1 = mid;
		}
	}
}
