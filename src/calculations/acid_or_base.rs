//This file defines the AcidOrBase type
//It would be beneficial to read main.rs first for context
use std::f64::INFINITY;
///AcidOrBase consists of its type (acid or base), the k value(s) as a vector, and the concentration
pub struct AcidOrBase {
	pub is_acid: bool,
	//if it's not an acid then it's a base, hence bool
	pub k_values: Vec<f64>,
	pub concentration: f64,
}
impl AcidOrBase {
	///The basic constructor takes as input whether it's an acid or base, and the k values
	///It does not take concentration, as this will be set later
	pub fn new(is_acid: bool, k_values: Vec<f64>) -> AcidOrBase {
		AcidOrBase {
			is_acid,
			k_values,
			concentration: 0.0,
		}
	}
	///dictionary_lookup has a list of known acids and bases
	///It takes as input a name, and if the name is in the list it'll return the corresponding
	///AcidOrBase, otherwise it returns none
	pub fn dictionary_lookup(name: String) -> Option<AcidOrBase> {
		//This function looks through a list of known compounds
		match name.as_str() {
			//Strong acids
			"hi" | "hbr" | "hclo4" | "hcl" | "hclo3" | "hno3" => {
				Some(AcidOrBase::new(true, vec![INFINITY]))
			}
			"h2seo4" => Some(AcidOrBase::new(true, vec![INFINITY, 2.2e-2])),
			"h2so4" => Some(AcidOrBase::new(true, vec![INFINITY, 1.1e-2])),
			//Strong bases
			"naoh" | "koh" | "lioh" | "rboh" | "csoh" => Some(AcidOrBase::new(false, vec![INFINITY])),
			"ca(oh)2" | "ba(oh)2" | "sr(oh)2" => Some(AcidOrBase::new(false, vec![INFINITY; 2])),
			//Weak acids
			"ch3cooh" => Some(AcidOrBase::new(true, vec![1.8e-5])),
			"hc3h3o2" => Some(AcidOrBase::new(true, vec![5.5e-5])),
			"h3aso4" => Some(AcidOrBase::new(true, vec![6.0e-3, 1.0e-7, 3.2e-12])),
			"h3aso3" => Some(AcidOrBase::new(true, vec![6.6e-10])),
			"c6h5cooh" => Some(AcidOrBase::new(true, vec![6.3e-5])),
			"ch2brcooh" => Some(AcidOrBase::new(true, vec![1.3e-3])),
			"hc4h7o2" => Some(AcidOrBase::new(true, vec![1.5e-5])),
			"h2co3" => Some(AcidOrBase::new(true, vec![4.4e-7, 4.7e-11])),
			"ch2clcooh" => Some(AcidOrBase::new(true, vec![1.4e-3])),
			"hclo2" => Some(AcidOrBase::new(true, vec![1.1e-2])),
			"h3c6h5o7" => Some(AcidOrBase::new(true, vec![7.4e-4, 1.7e-5, 4.0e-7])),
			"hocn" => Some(AcidOrBase::new(true, vec![3.5e-4])),
			"chcl2cooh" => Some(AcidOrBase::new(true, vec![5.5e-2])),
			"ch2fcooh" => Some(AcidOrBase::new(true, vec![2.6e-3])),
			"hchooh" => Some(AcidOrBase::new(true, vec![1.8e-4])),
			"hn3" => Some(AcidOrBase::new(true, vec![1.9e-5])),
			"hcn" => Some(AcidOrBase::new(true, vec![6.2e-10])),
			"hf" => Some(AcidOrBase::new(true, vec![6.6e-4])),
			"h2o2" => Some(AcidOrBase::new(true, vec![2.2e-12])),
			"h2se" => Some(AcidOrBase::new(true, vec![1.3e-4, 1.0e-11])),
			"h2s" => Some(AcidOrBase::new(true, vec![1.0e-7, 1.0e-19])),
			"h2te" => Some(AcidOrBase::new(true, vec![2.3e-3, 1.6e-11])),
			"hobr" => Some(AcidOrBase::new(true, vec![2.5e-9])),
			"hocl" => Some(AcidOrBase::new(true, vec![2.9e-8])),
			"hoi" => Some(AcidOrBase::new(true, vec![2.3e-11])),
			"hon=noh" => Some(AcidOrBase::new(true, vec![8.9e-8, 4.0e-12])),
			"hio3" => Some(AcidOrBase::new(true, vec![1.6e-1])),
			"ch2icooh" => Some(AcidOrBase::new(true, vec![6.7e-4])),
			"h2c3h2o4" => Some(AcidOrBase::new(true, vec![1.5e-3, 2.0e-6])),
			"hno2" => Some(AcidOrBase::new(true, vec![7.2e-4])),
			"h2c2o4" => Some(AcidOrBase::new(true, vec![5.4e-2, 5.3e-5])),
			"c6h5oh" => Some(AcidOrBase::new(true, vec![1.0e-10])),
			"hc8h7o2" => Some(AcidOrBase::new(true, vec![4.9e-5])),
			"h3po4" => Some(AcidOrBase::new(true, vec![7.1e-3, 6.3e-8, 4.2e-13])),
			"h3po3" => Some(AcidOrBase::new(true, vec![3.7e-2, 2.1e-7])),
			"ch3ch2cooh" => Some(AcidOrBase::new(true, vec![1.3e-5])),
			"h4p2o7" => Some(AcidOrBase::new(true, vec![3.0e-2, 4.4e-3, 2.5e-7, 5.6e-10])),
			"h2seo3" => Some(AcidOrBase::new(true, vec![2.3e-3, 5.4e-9])),
			"h2c4h4o4" => Some(AcidOrBase::new(true, vec![6.2e-5, 2.3e-6])),
			"h2so3" => Some(AcidOrBase::new(true, vec![1.3e-2, 6.2e-8])),
			"c6h5sh" => Some(AcidOrBase::new(true, vec![3.2e-7])),
			"ccl3cooh" => Some(AcidOrBase::new(true, vec![3.0e-1])),
			//Weak bases
			"nh3" => Some(AcidOrBase::new(false, vec![1.8e-5])),
			"c6h5nh2" => Some(AcidOrBase::new(false, vec![7.4e-10])),
			"c18h21o3n" => Some(AcidOrBase::new(false, vec![8.9e-7])),
			"(c2h5)2nh" => Some(AcidOrBase::new(false, vec![6.9e-4])),
			"(ch3)2nh" => Some(AcidOrBase::new(false, vec![5.9e-4])),
			"c2h5nh2" => Some(AcidOrBase::new(false, vec![4.3e-4])),
			"nh2nh2" => Some(AcidOrBase::new(false, vec![8.5e-7, 8.9e-16])),
			"nh2oh" => Some(AcidOrBase::new(false, vec![9.1e-9])),
			"c9h7n" => Some(AcidOrBase::new(false, vec![2.5e-9])),
			"ch3nh2" => Some(AcidOrBase::new(false, vec![4.2e-4])),
			"c17h19o3n" => Some(AcidOrBase::new(false, vec![7.4e-7])),
			"c5h11n" => Some(AcidOrBase::new(false, vec![1.3e-3])),
			"c5h5n" => Some(AcidOrBase::new(false, vec![1.5e-9])),
			"quinoline" => Some(AcidOrBase::new(false, vec![6.3e-10])),
			"c6h15o3n" => Some(AcidOrBase::new(false, vec![5.8e-7])),
			"(c2h5)3n" => Some(AcidOrBase::new(false, vec![5.2e-4])),
			"(ch3)3n" => Some(AcidOrBase::new(false, vec![6.3e-5])),
			_ => None, //If it didn't match, we'll have to ask the user what it is
		}
	}
	///h_contribution determines how much a particular acid or base
	///Affects the difference [H]-[OH] at a given ph
	///In general acids will return positive values, and bases will be negative
	///Furthermore, bases are more effective in acidic solutions, and vice versa
	pub fn h_contribution(&self, ph: f64) -> f64 {
		//We will begin by finding relevant ion concentration
		//If it's an acid we wish to know [H] otherwise we want [OH]
		let ion_concentration = if self.is_acid {
			10_f64.powf(-ph) //[H]
		} else {
			10_f64.powf(ph - 14.0) //[OH]
		};
		//Now we calculate how much ion is being added to solution
		//The formula for doing this takes a lot of math to derive
		//Just trust me that this is how to compute it
		let n = self
			.k_values
			.iter()
			.rposition(|x| x.is_infinite())
			.map_or(0, |x| x + 1);
		//n represents how many times it dissociates completely
		let mut total_dissociation = n as f64 * self.concentration;
		total_dissociation += self.concentration / self
			.k_values
			.iter()
			.skip(n)
			.rev()
			.fold(1.0, move |acc, k| acc * k / ion_concentration + 1.0)
			* self
				.k_values
				.iter()
				.skip(n)
				.enumerate()
				.rev()
				.fold(0.0, |acc, (i, k)| {
					(acc + 1.0 + i as f64) * k / ion_concentration
				});
		if self.is_acid {
			total_dissociation
		} else {
			-total_dissociation
		}
	}
}
